from django.shortcuts import render
from django.views.generic import ListView
from blog.models import Article

# user = context['user']
# query = self.request.GET.get('q')
# items_exists = Item.objects.filter(user=user).exits()

# Create your views here.
class SearchArticleListView(ListView):
    template_name = "search/search_results.html"

    def get_queryset(self):
    #def get_context_data(self, *args, **kwargs):
    #    context = super(SearchArticleListView, self).get_context_data(*args, **kwargs)
        query = self.request.GET.get('q')
        #print("Query", query)
        if query:
            qs = Article.objects.all().search(query)
        else:
            qs = Article.objects.none()
        return qs
        #if qs.exists():
        #    context['articles'] = qs
        #return context
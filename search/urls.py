from django.conf.urls import url
from .views import SearchArticleListView


urlpatterns = [
    #url(r'^(?P<username>[\w-]+)/$', ProfileDetailView.as_view(), name='detail')
    url(r'^$', SearchArticleListView.as_view(), name='index')
]
from django.conf.urls import url

from .views import (
    ArticleListView,
    AllArticleListView,
    ArticleDetailView,
    ArticleDeleteView,
    ArticleUpdateView,
    ArticleCreateView,

    CategoryCreateView,
    CategoryUpdateView,
    CategoryDeleteView
)

urlpatterns = [
    #url(r'^$', ArticleListView.as_view(), name='list-all'),
    url(r'^$', AllArticleListView.as_view(), name='list-all'),
    url(r'^new-article/$', ArticleCreateView.as_view(), name='article-new'),
    url(r'^new-category/$', CategoryCreateView.as_view(), name='category-new'),
    url(r'^(?P<slug>[-\w]+)/update/$', CategoryUpdateView.as_view(), name='category-edit'),
    url(r'^(?P<slug>[-\w]+)/delete/$', CategoryDeleteView.as_view(), name='category-delete'),
    url(r'^(?P<category>[-\w]+)/$', ArticleListView.as_view(), name='list-specific'),
    url(r'^(?P<category>[-\w]+)/page/(?P<page>\d+)/$', ArticleListView.as_view(), name='list-page'),
    url(r'^(?P<category>[-\w]+)/(?P<slug>[-\w]+)/update/$', ArticleUpdateView.as_view(), name='article-edit'),
    url(r'^(?P<category>[-\w]+)/(?P<slug>[-\w]+)/delete/$', ArticleDeleteView.as_view(), name='article-delete'),
    url(r'^(?P<category>[-\w]+)/(?P<slug>[-\w]+)/$', ArticleDetailView.as_view(), name='article-view')
]
from django import template
from blog.models import Article


register = template.Library()

@register.inclusion_tag('blog/show_blog_entries.html')
def show_articles(value):
    #print("category: ", value)
    articles = Article.objects.filter(
                 category__slug__iexact=value
            ).order_by('-posted')[0:5]
    return { 'articles' : articles }

@register.inclusion_tag('blog/show_blog_entries.html')
def show_recent_articles():
    #print("category: ", value)
    articles = Article.objects.all().order_by('-posted')[0:5]
    #articles = Article.objects.all().order_by('-posted')[:3]
    return { 'articles' : articles }

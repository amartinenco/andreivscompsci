from django import template
from blog.models import Category, Article
from django.db.models import Count


register = template.Library()

@register.inclusion_tag('blog/show_blog_categories.html')
def show_categories():
    categories = Category.objects.all()
    #print(categories)
    return { 'categories' : categories }

@register.inclusion_tag('blog/show_blog_categories_widget.html')
def show_categories_widget():
    #values() : specifies which columns are going to be used to "group by"
    #annotate() : specifies an operation over the grouped values
    #order_by() - for exclusion possible default ordering that can cause not needed fields inclusion in SELECT and GROUP BY

    #num_art_in_cat = Article.objects.all().values('category__slug', 'category__title').order_by().annotate(Count('category'))

    # SELECT c.category_name, COUNT(p.product_name)
    # FROM products p
    # FULL OUTER JOIN categories c ON p.category_id = c.category_id
    # GROUP BY c.category_name, p.category_id

    #prefetch_related

    #num_art_in_cat = Category.objects.all().values('article__title').order_by().annotate(Count('article'))
    #num_art_in_cat = Category.objects.only('title', 'slug')

    num_art_in_cat = Category.objects.all().values('slug', 'title').annotate(Count('article__title', distinct=True))
    #print(num_art_in_cat)
    return {'num_art_in_cat': num_art_in_cat}



# @register.inclusion_tag('blog/show_blog_categories_widget.html')
# def show_categories_widget():
#     categories = Category.objects.all()
#     if len(categories) % 2 == 0:
#     return { 'categories' : categories }

# def split_list(a_list):
#     half = len(a_list)/2
#     return a_list[:half], a_list[half:]
#
# A = [1,2,3,4,5,6]
# B, C = split_list(A)
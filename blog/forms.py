from django import forms
from .models import Article, Category
from django_summernote.widgets import SummernoteWidget, SummernoteInplaceWidget


class ArticleCreateForm(forms.ModelForm):
    #email           = forms.EmailField()
    #category         = forms.CharField(required=False, validators=[validate_category])
    #body = forms.CharField(widget=SummernoteWidget())
    description = forms.CharField(
        max_length = 150,
        widget = forms.Textarea
    )

    class Meta:
        model = Article
        fields = [
            'title',
            'description',
            'slug',
            'category',
            'body',
        ]
        widgets = {
            'body' : SummernoteWidget()
        }
    # def clean_name(self):
    #     name = self.cleaned_data.get("name")
    #     if name == "Hello":
    #         raise forms.ValidationError("Not a valid name")
    #     return name


class CategoryCreateForm(forms.ModelForm):

    class Meta:
        model = Category
        fields = [
            'title',
            'slug',
            'category_logo',
            'body'
        ]
from django.conf import settings
from django.core.urlresolvers import reverse
from django.db import models
from django.db.models import Q
from django_bleach.models import BleachField


class ArticleQuerySet(models.query.QuerySet):
    def search(self, query): #Article.objects.all().search(query) #Article.objects.filter(something).search()
        if query:
            query = query.strip()
            return self.filter(
                Q(title__icontains=query)|
                Q(title__iexact=query)|
                Q(description__icontains=query)|
                Q(description__iexact=query) |
                Q(category__title__icontains=query) |
                Q(category__title__iexact=query)
            )
        return self

#managers are actually used to override database operations models are involved
class ArticleManager(models.Manager):

    def get_queryset(self):
        return ArticleQuerySet(self.model, using=self._db)

    def search(self, query): #Article.objects.search()
        return self.get_queryset().search(query)


class Category(models.Model):
    title = models.CharField(max_length=50, db_index=True, unique=True)
    slug  = models.SlugField(max_length=75, db_index=True, unique=True)
    category_logo = models.ImageField()
    body = models.TextField()

    def __str__(self):
        return self.title

    class Meta:
        verbose_name_plural = "Categories"

    def get_absolute_url(self):
        return reverse('blog:list-all')


class Article(models.Model):
    # associations
    author  = models.ForeignKey(settings.AUTH_USER_MODEL)
    title   = models.CharField(max_length=150, unique=True)
    description = models.CharField(max_length=200)
    slug    = models.SlugField(max_length=175, unique=True)
    category = models.ForeignKey(Category, on_delete=models.SET_DEFAULT, default=1)
    #body    = models.TextField()
    body    = BleachField()
    posted  = models.DateTimeField(db_index=True, auto_now_add=True)

    objects = ArticleManager()

    def __str__(self):
        return self.title

    def get_absolute_url(self): #get_absolute_url
        #return f"/restaurants/{self.slug}"
        return reverse('blog:article-view', kwargs={'slug': self.slug,
                                                'category' : self.category.slug})


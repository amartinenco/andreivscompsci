# -*- coding: utf-8 -*-
# Generated by Django 1.11.3 on 2017-08-19 13:48
from __future__ import unicode_literals

from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
        ('blog', '0004_auto_20170819_0917'),
    ]

    operations = [
        migrations.AlterField(
            model_name='article',
            name='category',
            field=models.ForeignKey(default=8, on_delete=django.db.models.deletion.SET_DEFAULT, to='blog.Category'),
        ),
    ]

from django.shortcuts import render
from django.views.generic import ListView, DetailView, UpdateView, DeleteView, CreateView
from django.db.models import Q
from django.contrib.auth.mixins import LoginRequiredMixin, UserPassesTestMixin
from .models import Article, Category
from django.shortcuts import render, get_object_or_404, redirect
from django.contrib import messages
from django.http import Http404
from django.views.generic.edit import DeleteView
from django.core.urlresolvers import reverse_lazy
from .forms import ArticleCreateForm, CategoryCreateForm
from django.http import HttpResponseNotAllowed
from django.contrib.auth.mixins import PermissionRequiredMixin


class AllArticleListView(ListView):

    template_name = "blog/all_articles.html"

    def get_queryset(self):
        queryset = Category.objects.all()
        return queryset


class ArticleListView(ListView):
    template_name = 'blog/article_list.html'
    paginate_by = 4

    def get_context_data(self, *args, **kwargs):
        context = super(ArticleListView, self).get_context_data(*args, **kwargs)

        category_slug = self.kwargs.get("category")
        if category_slug:
            category = Category.objects.get(slug=category_slug)
            if category:
                context['category'] = category.title
                context['cat_slug'] = category.slug
            else:
                raise Http404
        return context

    def get_queryset(self):
        #print(self.kwargs)
        category_slug = self.kwargs.get("category")
        if category_slug:
            category = Category.objects.filter(slug=category_slug).exists();
            if category:
                #print("Category slug: ", category_slug)
                queryset = Article.objects.filter(
                    category__slug__iexact=category_slug
                ).order_by('-posted')
            else:
                raise Http404
        return queryset

class ArticleDetailView(DetailView):

    def get_queryset(self):
        #print(self.kwargs)
        return Article.objects.filter(category__slug=self.kwargs['category'], slug=self.kwargs['slug'])


class ArticleCreateView(LoginRequiredMixin, PermissionRequiredMixin, CreateView):
    form_class = ArticleCreateForm
    template_name = 'blog/article_form.html'
    permission_required = 'blog.add_article'

    def form_valid(self, form):
        form.instance.author = self.request.user
        return super(ArticleCreateView, self).form_valid(form)


class ArticleUpdateView(LoginRequiredMixin, PermissionRequiredMixin, UpdateView):

    form_class = ArticleCreateForm
    template_name = 'blog/article_form.html'
    permission_required = 'blog.change_article'

    def get_queryset(self):
        return Article.objects.filter(author=self.request.user)


class ArticleDeleteView(LoginRequiredMixin, PermissionRequiredMixin, DeleteView):

    model = Article
    success_url = reverse_lazy('profiles:index')
    permission_required = 'blog.delete_article'

    def get(self, *args, **kwargs):
        # return self.post(*args, **kwargs)
        return HttpResponseNotAllowed(['POST'])

# Category views

class CategoryCreateView(LoginRequiredMixin, PermissionRequiredMixin, CreateView):
    form_class = CategoryCreateForm
    template_name = 'blog/category_form.html'
    permission_required = 'blog.add_category'

    def form_valid(self, form):
        form.instance.author = self.request.user
        return super(CategoryCreateView, self).form_valid(form)


class CategoryUpdateView(LoginRequiredMixin, PermissionRequiredMixin, UpdateView):
    form_class = CategoryCreateForm
    template_name = 'blog/category_form.html'
    permission_required = 'blog.change_category'

    def get_queryset(self):
        return Category.objects.all()


class CategoryDeleteView(LoginRequiredMixin, PermissionRequiredMixin, DeleteView):

    model = Category
    success_url = reverse_lazy('profiles:index')
    permission_required = 'blog.delete_category'

    def get(self, *args, **kwargs):
        #return self.post(*args, **kwargs)
        return HttpResponseNotAllowed(['POST'])


        #def get_object(self, queryset=None):
        #obj = super(ArticleDeleteView, self).get_object()
        #print("Obj owner: ", obj.owner)
        #print("Obj request user: ", self.request.user)
        #if not obj.author == self.request.user:
        #    raise Http404
        #account = Article.objects.get(id=obj.article.id)
        #return obj


            # class ArticleDeleteView(LoginRequiredMixin, DeleteView):
#
#     model = Article
#
#     def get_queryset(self):
#         qs = super(ArticleDeleteView, self).get_queryset()
#         print(self.request.pk)
#         return qs.filter(pk=self.request.pk)

#def post_delete(request, id=None):
#    instance = get_object_or_404(Article, id=id)
#    instance.delete()
#    messages.success(request, "Successfully deleted")
#    return redirect("posts:list")




# def delete_song(request, album_id, song_id):
#     album = get_object_or_404(Album, pk=album_id)
#     song = Song.objects.get(pk=song_id)
#     song.delete()
#     return render(request, 'music/detail.html', {'album': album})

#
# from django.views.generic.base import View
#
# class DeleteView(View):
#     def post(self, request *args, **kwargs):
#         article = get_object_or_404(id=request.POST['article_id'])
#         # perform some validation,
#         # like can this user delete this article or not, etc.
#         # if validation is successful, delete the article
#         article.delete()
#         return HttpResponseRedirect('/')
# Your template should be like this:
#
# <form action="/url/to/delete/view/" method="POST">
#     {% csrf_token %}
#     <input type="hidden" value="{{ article.id }}">
#     <input type="submit" value="Delete">
# </form>

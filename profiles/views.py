from django.shortcuts import render, get_object_or_404
from django.contrib.auth import get_user_model
from django.views.generic import DetailView
from django.http import Http404
from django.contrib.auth.mixins import LoginRequiredMixin
from django.contrib.auth.mixins import PermissionRequiredMixin
from blog.models import Category
from projects.models import Projects
import json
import urllib
from django.conf import settings
from django.contrib import messages


from django.contrib.auth.views import LoginView

User = get_user_model()

# class ProfileDetailView(DetailView):
#     template_name = 'profiles/user.html'
#
#     def get_object(self):
#         username = self.kwargs.get("username")
#         if username is None:
#             raise Http404
#         return get_object_or_404(User, username__iexact=username, is_active=True)



# class CheckBlogPermissions(UserPassesTestMixin):
#
#     permissions = ['blog.delete_article', 'blog.add_article', 'blog.change_article']
#
#     def test_func(self):
#         if self.request.user is not None and self.request.user.is_authenticated():
#             for token in self.permissions:
# CheckBlogPermissions,

class CustomLoginView(LoginView):

    def post(self, request, *args, **kwargs):
        form = self.get_form()

        ''' Begin reCAPTCHA validation '''
        recaptcha_response = self.request.POST.get('g-recaptcha-response')
        url = 'https://www.google.com/recaptcha/api/siteverify'
        values = {
            'secret': settings.GOOGLE_RECAPTCHA_SECRET_KEY,
            'response': recaptcha_response
        }
        data = urllib.parse.urlencode(values).encode()
        req = urllib.request.Request(url, data=data)
        response = urllib.request.urlopen(req)
        result = json.loads(response.read().decode())
        if result['success']:
            if form.is_valid():
                return self.form_valid(form)
            else:
                return self.form_invalid(form)
        else:
            messages.error(self.request, 'Invalid reCAPTCHA. Please try again.')
            return self.form_invalid(form)



    def get_context_data(self, *args, **kwargs):
        context = super(CustomLoginView, self).get_context_data(*args, **kwargs)
        return context


class ProfileDetailView(LoginRequiredMixin, PermissionRequiredMixin, DetailView):
    template_name = 'profiles/user.html'
    permission_required = ('blog.add_article',
                           'blog.delete_article',
                           'blog.change_article',
                           'blog.add_category',
                           'blog.delete_category',
                           'blog.change_category',
                           'projects.delete_projects',
                           'projects.add_projects',
                           'projects.change_projects',
                           'about.add_about',
                           'about.delete_about',
                           'about.change_about'
                           )

    def get_object(self):
        # form = AuthenticationForm(self.request.POST)
        # if form.is_valid():
        #     ''' Begin reCAPTCHA validation '''
        #     recaptcha_response = self.request.POST.get('g-recaptcha-response')
        #     url = 'https://www.google.com/recaptcha/api/siteverify'
        #     values = {
        #         'secret': settings.GOOGLE_RECAPTCHA_SECRET_KEY,
        #         'response': recaptcha_response
        #     }
        #     data = urllib.parse.urlencode(values).encode()
        #     req = urllib.request.Request(url, data=data)
        #     response = urllib.request.urlopen(req)
        #     result = json.loads(response.read().decode())
        #
        #     print(recaptcha_response)
        #     print(result)
        #
        #     ''' End reCAPTCHA validation '''
        #     if result['success']:
        #         form.save()
        #         print("SUCCESS")
        #     else:
        #         print("FAIL")
        # else:
        #     print("NOT VALID")

        if self.request.user is not None and self.request.user.is_authenticated():
            return get_object_or_404(User, username__iexact=self.request.user.username, is_active=True)
        else:
            raise Http404


    def get_context_data(self, *args, **kwargs):
        context = super(ProfileDetailView, self).get_context_data(*args, **kwargs)

        if self.request.user is not None and self.request.user.is_authenticated():
            context['user'] = self.request.user
            context['categories'] = Category.objects.all()
            context['projects'] = Projects.objects.all()
        return context

# username = self.kwargs.get("username")
#         if username is None:
#             raise Http404
#         return get_object_or_404(User, username__iexact=username, is_active=True)


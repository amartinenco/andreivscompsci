from django import forms
from .models import Projects
from django_summernote.widgets import SummernoteWidget, SummernoteInplaceWidget


class ProjectsCreateForm(forms.ModelForm):
    description = forms.CharField(
        max_length = 150,
        widget = forms.Textarea
    )

    class Meta:
        model = Projects
        fields = [
            'logo',
            'title',
            'slug',
            'description',
            'body',
        ]
        widgets = {
            'body' : SummernoteWidget()
        }

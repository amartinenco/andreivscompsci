from django.shortcuts import render
from .models import Projects
from django.http import Http404
from django.views.generic import ListView, DetailView, UpdateView, DeleteView, CreateView
from django.db.models import Q
from django.contrib.auth.mixins import LoginRequiredMixin, UserPassesTestMixin
from django.http import HttpResponseNotAllowed
from .forms import ProjectsCreateForm
from django.core.urlresolvers import reverse_lazy
from django.shortcuts import render, get_object_or_404, redirect
from django.contrib.auth.mixins import PermissionRequiredMixin


class ProjectListView(ListView):
    template_name = 'projects/projects_list.html'

    def get_queryset(self):
        queryset = Projects.objects.all() #.order_by('-posted')
        return queryset


class ProjectDetailView(DetailView):

    def get_queryset(self):
        return Projects.objects.filter(slug=self.kwargs['slug'])


class ProjectCreateView(LoginRequiredMixin, PermissionRequiredMixin, CreateView):
    form_class = ProjectsCreateForm
    template_name = 'projects/projects_form.html'
    permission_required = ('projects.add_projects')


class ProjectUpdateView(LoginRequiredMixin, PermissionRequiredMixin, UpdateView):
    form_class = ProjectsCreateForm
    template_name = 'projects/projects_form.html'
    permission_required = ('projects.change_projects')

    def get_queryset(self):
        return Projects.objects.all()


class ProjectDeleteView(LoginRequiredMixin, PermissionRequiredMixin, DeleteView):
    model = Projects
    success_url = reverse_lazy('projects:index')
    permission_required = ('projects.delete_projects')

    def get(self, *args, **kwargs):
        # return self.post(*args, **kwargs)
        return HttpResponseNotAllowed(['POST'])
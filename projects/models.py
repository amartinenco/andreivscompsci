from django.conf import settings
from django.core.urlresolvers import reverse
from django.db import models
from django.db.models import Q
from django_bleach.models import BleachField
from django.db.models.signals import post_delete
from django.dispatch.dispatcher import receiver


class Projects(models.Model):
     logo           = models.ImageField()
     title          = models.CharField(max_length=30, unique=True)
     slug           = models.SlugField(max_length=45, unique=True)
     description    = models.CharField(max_length=200)
     body           = BleachField()

     def __str__(self):
         return self.title

     def get_absolute_url(self):
         return reverse('projects:projects-detail', kwargs={'slug': self.slug})


@receiver(post_delete, sender=Projects)
def photo_post_delete_handler(sender, **kwargs):
    photo = kwargs['instance']
    storage, path = photo.logo.storage, photo.logo.path
    storage.delete(path)
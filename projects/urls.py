from django.conf.urls import url
from . import views


urlpatterns = [
    url(r'^$', views.ProjectListView.as_view(), name='index'),
    url(r'^new-project/$', views.ProjectCreateView.as_view(), name='projects-new'),
    url(r'^(?P<slug>[-\w]+)/update/$', views.ProjectUpdateView.as_view(), name='projects-edit'),
    url(r'^(?P<slug>[-\w]+)/delete/$', views.ProjectDeleteView.as_view(), name='projects-delete'),
    url(r'^(?P<slug>[-\w]+)/$', views.ProjectDetailView.as_view(), name='projects-detail')
]
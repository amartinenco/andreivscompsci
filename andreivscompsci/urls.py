"""andreivscompsci URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/1.11/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  url(r'^$', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  url(r'^$', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.conf.urls import url, include
    2. Add a URL to urlpatterns:  url(r'^blog/', include('blog.urls'))
"""
#from django.conf.urls import url
#from django.contrib import admin

#urlpatterns = [
#    url(r'^admin/', admin.site.urls),
#]
from django.conf.urls import url, include
from django.contrib import admin
from django.views.generic import TemplateView
from django.contrib.auth.views import LoginView, LogoutView

from django.conf import settings
from django.conf.urls.static import static
from profiles.views import CustomLoginView

urlpatterns = [
    #url(r'^admin/', admin.site.urls),
    #url(r'^$', TemplateView.as_view(template_name='home.html'), name='home'),
    url(r'^$', TemplateView.as_view(template_name='home_blog.html'), name='home'),
    #url(r'^about/$', TemplateView.as_view(template_name='about.html'), name='about'),
    url(r'^about/', include('about.urls', namespace='about')),
    url(r'^projects/', include('projects.urls', namespace='projects')),
    url(r'^contact/$', TemplateView.as_view(template_name='contact.html'), name='contact'),
    #url(r'^test/$', TemplateView.as_view(template_name='test.html'), name='test'),
    url(r'^blog/', include('blog.urls', namespace='blog')),
    url(r'^search/', include('search.urls', namespace='search')),
    url(r'^profile/', include('profiles.urls', namespace='profiles')),
    #url(r'^login/$', LoginView.as_view(), name='login'),
    url(r'^login/$', CustomLoginView.as_view(), name='login'),
    url(r'^logout/$', LogoutView.as_view(next_page='/'), name='logout'),

    url(r'^summernote/', include('django_summernote.urls')),
]

if settings.DEBUG:
    urlpatterns += static(settings.STATIC_URL, document_root=settings.STATIC_ROOT)
    urlpatterns += static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)



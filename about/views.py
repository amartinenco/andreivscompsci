from django.shortcuts import render
from .models import About
from django.http import Http404
from django.views.generic import ListView, DetailView, UpdateView, DeleteView, CreateView
from django.db.models import Q
from django.contrib.auth.mixins import LoginRequiredMixin, UserPassesTestMixin
from django.http import HttpResponseNotAllowed
from .forms import AboutCreateForm
from django.core.urlresolvers import reverse_lazy
from django.shortcuts import render, get_object_or_404, redirect
from django.contrib.auth.mixins import PermissionRequiredMixin


# Create your views here.
def about(request):
    try:
         bio = About.objects.first()
    except About.DoesNotExist:
        raise Http404("Biography does not exist")

    return render(request, 'about/about.html', {'bio': bio})


class AboutCreateView(LoginRequiredMixin, PermissionRequiredMixin, CreateView):
    form_class = AboutCreateForm
    template_name = 'about/about_form.html'
    permission_required = ('about.add_about')

    def form_valid(self, form):
        form.instance.author = self.request.user
        return super(AboutCreateView, self).form_valid(form)

    #def get(self, *args, **kwargs):
    #    return HttpResponseNotAllowed(['POST'])


class AboutUpdateView(LoginRequiredMixin, PermissionRequiredMixin, UpdateView):

    form_class = AboutCreateForm
    template_name = 'about/about_form.html'
    permission_required = ('about.change_about')

    def get_object(self):
        obj = About.objects.first()
        if obj is None:
            raise Http404
        return obj

    #def get(self, *args, **kwargs):
    #    return HttpResponseNotAllowed(['POST'])

    #def get_object(self):
        #return About.objects.get(pk=self.request.GET.get('pk'))
        #first = About.objects.first()
        #if first.exists():
        #print(first)
        #obj = get_object_or_404(About, pk=1)

        #return About.objects.get(pk=About.objects.first().id)
        #return obj
    #def get_queryset(self):
    #    return About.objects.first()

    #def get_queryset(self):
    #    return Article.objects.filter(author=self.request.user)


class AboutDeleteView(LoginRequiredMixin, PermissionRequiredMixin, DeleteView):

    model = About
    success_url = reverse_lazy('home')
    permission_required = ('about.delete_about')

    def get_object(self):
        obj = About.objects.first()
        if obj is None:
            raise Http404
        return obj

    def get(self, *args, **kwargs):
        return HttpResponseNotAllowed(['POST'])
from django.conf.urls import url
from . import views


urlpatterns = [
    url(r'^$', views.about, name='biography'),
    url(r'^new-bio/$', views.AboutCreateView.as_view(), name='about-new'),
    url(r'^update/$', views.AboutUpdateView.as_view(), name='about-edit'),
    url(r'^delete/$', views.AboutDeleteView.as_view(), name='about-delete'),
]
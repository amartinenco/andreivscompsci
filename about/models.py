from django.db import models
from django_bleach.models import BleachField
from django.core.exceptions import ValidationError
from django.contrib import admin
from django.core.urlresolvers import reverse


class About(models.Model):

    title = models.CharField(max_length=150, unique=True)
    #face = models.ImageField()
    body = BleachField()

    def __str__(self):
        return self.title

    def get_absolute_url(self): #get_absolute_url
        #return f"/restaurants/{self.slug}"
        return reverse('about:biography')


class SettingAdmin(admin.ModelAdmin):
    def has_add_permission(self, request):
        base_add_permission = super(SettingAdmin, self).has_add_permission(request)
        if base_add_permission:
            # if there's already an entry, do not allow adding
            count = About.objects.all().count()
            if count == 0:
                return True
        return False

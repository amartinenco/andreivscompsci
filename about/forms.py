from django import forms
from .models import About
from django_summernote.widgets import SummernoteWidget, SummernoteInplaceWidget


class AboutCreateForm(forms.ModelForm):

    class Meta:
        model = About
        fields = [
            'title',
            'body',
        ]
        widgets = {
            'body' : SummernoteWidget()
        }